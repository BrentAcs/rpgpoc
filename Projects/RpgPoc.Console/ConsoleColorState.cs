﻿using System;
using System.Collections.Generic;

namespace RpgPoc.ConsoleApp
{
  public static class ConsoleColorState
  {
    private class State
    {
      public State()
        : this(Console.ForegroundColor, Console.BackgroundColor)
      {
      }

      public State(ConsoleColor foreground, ConsoleColor background)
      {
        Foreground = foreground;
        Background = background;
      }
      public ConsoleColor Foreground { get; set; }
      public ConsoleColor Background { get; set; }
    }

    private static readonly Stack<State> _stack = new Stack<State>();

    public static void Push()
    {
      _stack.Push(new State());
    }

    public static void Pop()
    {
      State state = _stack.Pop();

      Console.ForegroundColor = state.Foreground;
      Console.BackgroundColor = state.Background;
    }
  }
}