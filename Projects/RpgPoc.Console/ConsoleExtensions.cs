﻿using System;
using System.Drawing;

namespace RpgPoc.Concole
{
  public class Tile
  {
    public Point Location { get; set; }
    public char DisplayValue { get; set; }
    public ConsoleColor BackColor { get; set; }
    public ConsoleColor ForeColor { get; set; }
  }

  //public static class ConsoleExtensions
  //{
  //  public static void Render( this Tile tile)
  //  {
  //    Console.CursorLeft = tile.Location.X;
  //    Console.CursorTop = tile.Location.Y;
  //    Console.ForegroundColor = tile.ForeColor;
  //    Console.Write(tile.DisplayValue);
  //  }
  //}
}