﻿using System;
using RpgPoc.Core.Characters;

namespace RpgPoc.ConsoleApp
{
  public static class ConsoleRenderExtensions
  {
    public static void Render(this Attributes attributes)
    {
      ConsoleColor currentColor = Console.ForegroundColor;

      Render("Str", attributes[AttributeNames.Strength]);
      Render("Dex", attributes[AttributeNames.Dexterity]);
      Render("Int", attributes[AttributeNames.Intelligence]);
      Render("Wis", attributes[AttributeNames.Wisdom]);
      Render("Chr", attributes[AttributeNames.Charisma]);
      Render("Con", attributes[AttributeNames.Constitution]);
    }

    public static void Render(string name, AttributePoint attribute)
    {
      char modiferSign = (attribute.Modifier > 0 ? '+' : (attribute.Modifier < 0 ? '-' : ' '));

      Console.WriteLine($"{name}: {attribute.BaseValue, 2}  {modiferSign}{Math.Abs(attribute.Modifier)}");
    }

    public static void Render(this HitPoints hitPoints)
    {
      Console.WriteLine($"Hit Points: {hitPoints.Current}/{hitPoints.Maximum}");
    }
  }
}