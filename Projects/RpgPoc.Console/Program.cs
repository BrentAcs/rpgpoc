﻿using Microsoft.Extensions.Configuration;
using RpgPoc.Core.Characters;
using RpgPoc.Core.Equipment;
using RpgPoc.Core.Extensions;
using RpgPoc.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using MongoDB.Driver;
using RpgPoc.Core;
using RpgPoc.Core.Storage;

namespace RpgPoc.ConsoleApp
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WindowWidth = 120;
      Console.WindowHeight = 40;

      try
      {
        //attrs.ToJsonFile(@"..\..\..\..\..\output\attrs.json");

        //PlayerCharacter pc = new PlayerCharacter();
        //pc.ToJsonFile(@"..\..\..\..\..\output\player-character.json");

        //MasterEquipmentCollection collection = BuildMasterEquipmentCollection(@"..\..\..\..\..\output\all-gear.json");
        //collection.ToJsonFile(@"..\..\..\..\..\output\all-gear.json");

        int attributeCount = Enum.GetValues(typeof(AttributeNames)).Length;
        NonPlayerCharacterTemplate kobaldNpcTemplate = new NonPlayerCharacterTemplate(
          name: "Kobold",
          level: 1,
          hitDice: new DiceRoll(Dice.D4),
          xpValue: 10,
          encounterSize: new DiceRoll(Dice.D4),
          str: 10,
          intel: 10,
          dex: 10,
          wis: 10,
          chr: 10,
          con: 10);
        NonPlayerCharacterTemplate goblinNpcTemplate = new NonPlayerCharacterTemplate(
          name: "Goblin",
          level: 1,
          hitDice: new DiceRoll(Dice.D6),
          xpValue: 15,
          encounterSize: new DiceRoll(Dice.D4),
          str: 11,
          intel: 10,
          dex: 11,
          wis: 10,
          chr: 10,
          con: 10);

        var npcTemlates = new NonPlayerCharacterTemplate[]
        {
          kobaldNpcTemplate,
          goblinNpcTemplate,
        };


        INonPlayerCharacterGenerator npcGenerator = new NonPlayerCharacterGenerator();
        var npcs = npcGenerator.GenerateEncounterGroup(goblinNpcTemplate).ToJson();

        Trace.WriteLine(npcs);
        File.WriteAllText(@"D:\Dev\rpgpoc\Output\encounter-npcs.json", npcs);


      }
      catch (Exception ex)
      {
        Trace.WriteLine(ex);
      }
    }

    private static void Out(string msg)
    {
      Console.WriteLine(msg);
    }

    public static List<PlayerCharacter> BuildExampleSampleCharacters(string saveFilename)
    {
      List<PlayerCharacter> pcs = new List<PlayerCharacter>
      {
        new PlayerCharacter()
        {
          Name = "Gotch Urarse",
          Class = CharacterClass.Fighter,
        },
        new PlayerCharacter()
        {
          Name = "Empathy Rests",
          Class = CharacterClass.Wizard,
        }
      };

      pcs[0].ApplyXp(5000);
      pcs[1].ApplyXp(20000);

      if (!string.IsNullOrEmpty(saveFilename))
      {
        pcs.ToJsonFile(saveFilename);
      }

      return pcs;
    }

    private static MasterEquipmentCollection BuildMasterEquipmentCollection(string saveFilename)
    {
      MasterEquipmentCollection collection = new MasterEquipmentCollection
      {
        Armor = new List<Armor>
        {
          new Armor { Name = "Robes", ArmorRating = 2, BasePrice = 10 },
          new Armor { Name = "Leather", ArmorRating = 4, BasePrice = 40 },
          new Armor { Name = "Scale", ArmorRating = 6, BasePrice = 100 },
          new Armor { Name = "Chain", ArmorRating = 8, BasePrice = 300 },
          new Armor { Name = "Plate", ArmorRating = 10, BasePrice = 500 },
        },
        Weapons = new List<Weapon>
        {
          new Weapon { Name = "Club", Damage = new DiceRoll(Dice.D4), BasePrice = 10 },
          new Weapon { Name = "Dagger", Damage = new DiceRoll(Dice.D4), BasePrice = 10 },
          new Weapon { Name = "Short Sword", Damage = new DiceRoll(Dice.D6), BasePrice = 20 },
          new Weapon { Name = "Mace", Damage = new DiceRoll(Dice.D6), BasePrice = 20 },
          new Weapon { Name = "Battle Axe", Damage = new DiceRoll(Dice.D8), BasePrice = 75 },
          new Weapon { Name = "Long Sword", Damage = new DiceRoll(Dice.D8), BasePrice = 75 },
          new Weapon { Name = "Great Sword", Damage = new DiceRoll(Dice.D10), BasePrice = 100 },
          new Weapon { Name = "Warhammer", Damage = new DiceRoll(Dice.D10), BasePrice = 100 },
        }
      };

      if (!string.IsNullOrEmpty(saveFilename))
      {
        collection.ToJsonFile(saveFilename);
      }

      return collection;
    }
  }

  public class EncounterTemplate
  {

  }

  #region
  //PlayerCharacter pc = new PlayerCharacter
  //{
  //  Name = "Gotch Urarse",
  //  //Name = "Empathy Rests",
  //  Class = CharacterClass.Ranger,
  //};
  //pc.AttributePoints[(int)AttributeNames.Strength].BaseValue = 11;
  //pc.AttributePoints[(int)AttributeNames.Dexterity].BaseValue = 12;
  //pc.AttributePoints[(int)AttributeNames.Intelligence].BaseValue = 13;
  //pc.AttributePoints[(int)AttributeNames.Wisdom].BaseValue = 14;
  //pc.AttributePoints[(int)AttributeNames.Charisma].BaseValue = 15;
  //pc.AttributePoints[(int)AttributeNames.Constitution].BaseValue = 16;

  //IConfigurationRoot configuration = new ConfigurationBuilder()
  //  .AddJsonFile(@"appsettings.json", optional: true)
  //  .Build();

  //string connectionString = configuration?[ConfigKeys.MongoDbConnectionString];
  //string databaseName = configuration?[ConfigKeys.MongoDbDatabaseName];

  //MongoRepository<PlayerCharacter>.DropCollection(connectionString, databaseName, CollectionNames.PlayerCharacters);

  //IDbStorage dbStorage = new DbStorage(connectionString, databaseName);
  //dbStorage.PlayerCharacterRepo.InsertOne(pc);

  //pc.Name = "Empathy Rests";
  //dbStorage.PlayerCharacterRepo.InsertOne(pc);

  //// will you fail? you should fail
  //dbStorage.PlayerCharacterRepo.InsertOne(pc);

  //var foundPC = dbStorage.PlayerCharacterRepo.FindPlayerCharacterByName("empathy rests");
  #endregion
}
