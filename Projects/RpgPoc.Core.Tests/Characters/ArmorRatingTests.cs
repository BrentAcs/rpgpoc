﻿using NUnit.Framework;
using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Tests.Characters
{
  [TestFixture]
  public class ArmorRatingTests
  {
    [Test]
    public void default_armor_rating_should_be_10()
    {
      PlayerCharacter pc = new PlayerCharacter();

      Assert.IsTrue(10 == pc.ArmorRating);
    }

    [Test]
    public void armor_rating_with_dexterity_of_6_should_8()
    {
      PlayerCharacter pc = new PlayerCharacter();
      pc.AttributePoints[(int)AttributeNames.Dexterity].BaseValue = 6;

      Assert.IsTrue( 8 == pc.ArmorRating );
    }

    [Test]
    public void armor_rating_with_dexterity_of_16_should_be_12()
    {
      PlayerCharacter pc = new PlayerCharacter();
      pc.AttributePoints[(int)AttributeNames.Dexterity].BaseValue = 16;

      Assert.IsTrue(13 == pc.ArmorRating);
    }
  }
}