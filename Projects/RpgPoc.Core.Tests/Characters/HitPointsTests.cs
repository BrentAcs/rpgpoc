﻿using NUnit.Framework;
using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Tests.Characters
{
  [TestFixture]
  public class HitPointsTests
  {
    [Test]
    public void can_create_hit_points_with_10_maximum()
    {
      var hp = new HitPoints(10);

      Assert.IsTrue(10 == hp.Maximum);
      Assert.IsTrue(10 == hp.Current);
    }

    [Test]
    public void will_reflect_correct_current_after_damage()
    {
      var hp = new HitPoints(10);
      hp.Damage(5);

      Assert.IsTrue(10 == hp.Maximum);
      Assert.IsTrue(5 == hp.Current);
    }

    [Test]
    public void will_reflect_correct_current_after_heal()
    {
      var hp = new HitPoints(10);
      hp.Damage(5);
      hp.Heal(5);

      Assert.IsTrue(10 == hp.Maximum);
      Assert.IsTrue(10 == hp.Current);
    }

    [Test]
    public void will_reflect_correct_current_after_heal_2()
    {
      var hp = new HitPoints(10);
      hp.Damage(5);
      hp.Heal(10);

      Assert.IsTrue(10 == hp.Maximum);
      Assert.IsTrue(10 == hp.Current);
    }
  }
}
