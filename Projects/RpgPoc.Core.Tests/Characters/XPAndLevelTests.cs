﻿using NUnit.Framework;
using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Tests.Characters
{
  [TestFixture]
  public class XPAndLevelTests
  {
    [Test]
    public void new_character_will_have_zero_xp()
    {
      PlayerCharacter pc = new PlayerCharacter();

      Assert.IsTrue(0 == pc.ExperiencePoints);
    }

    [Test]
    public void new_character_will_be_level_one()
    {
      PlayerCharacter pc = new PlayerCharacter();

      Assert.IsTrue( 1 == pc.Level );
    }
  }
}