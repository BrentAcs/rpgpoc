﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace RpgPoc.Core.Characters
{
  [DebuggerDisplay("Value: {BaseValue}, Modifer: {Modifier}")]
  public class AttributePoint
  {
    public const int MinimumValue = 3;
    public const int MaximumValue = 20;
    public const int Default = 10;

    public AttributePoint()
    {
    }

    public AttributePoint(int baseValue)
    {
      BaseValue = baseValue;
    }

    public static implicit operator AttributePoint(int baseValue)
    {
      return new AttributePoint(baseValue);
    }

    public int BaseValue { get; set; }
    [JsonIgnore]
    public int Modifier => ComputeModifier(BaseValue);

    public static int ComputeModifier(int score)
    {
      // source: https://www.dandwiki.com/wiki/MSRD:Ability_Scores
      int mod = (score / 2) - 5;
      return mod;
    }
  }
}