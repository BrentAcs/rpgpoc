﻿using System;
using System.Linq;

namespace RpgPoc.Core.Characters
{
  public enum AttributeNames
  {
    Strength = 0,
    Dexterity,
    Intelligence,
    Wisdom,
    Charisma,
    Constitution,
  };

  public class Attributes
  {
    private AttributePoint[] _attributePoints;

    public Attributes()
    {
      int attributeCount = Enum.GetValues(typeof(AttributeNames)).Length;
      _attributePoints = new AttributePoint[attributeCount];
      for (AttributeNames attributeName = AttributeNames.Strength; attributeName <= AttributeNames.Constitution; attributeName++)
      {
        _attributePoints[(int)attributeName] = 10;
      }
    }

    public AttributePoint this[AttributeNames attributeName] => _attributePoints[(int)attributeName];
  }
}