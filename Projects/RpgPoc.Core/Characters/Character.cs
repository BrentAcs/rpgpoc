﻿using System;
using System.Net.Configuration;
using System.Runtime.InteropServices;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using RpgPoc.Core.Events;

namespace RpgPoc.Core.Characters
{
  public abstract class Character
  {
    public const int BaseArmorRating = 10;
    public const int XpPerLevel = 2000;

    protected Character()
    {
      // TODO: address default hit points.
      HitPoints = new HitPoints(8);
      // TODO: address default attributes
      //Attributes = new Attributes();


      int attributeCount = Enum.GetValues(typeof(AttributeNames)).Length;
      AttributePoints = new AttributePoint[attributeCount];
      for (AttributeNames attributeName = AttributeNames.Strength; attributeName <= AttributeNames.Constitution; attributeName++)
      {
        AttributePoints[(int)attributeName] = 10;
      }
    }

    // Class
    //  - Fighter
    //  - Cleric
    //  - Wizard
    //  - Thief
    //  - Ranger
    // Race
    // Initiative
    // Alignment
    //  - needed ?
    // Experience Points
    // Level
    // Hit Dice
    // Equipment
    //  - Armor
    //    - Head
    //    - Body
    //    - Arms
    //    - Legs
    //    - Wrists
    //  - Weapon
    //    - Main hand
    //    - Off hand
    // Equipment object(s), lists


    public string Name { get; set; }
    public HitPoints HitPoints { get; set; }
    public int ArmorRating => CalculateArmorRating();

    public AttributePoint[] AttributePoints;

    private int CalculateArmorRating()
    {
      // TODO: determine armor rating of equipped gear, for now use base + Dexterity modifer
      int rating = BaseArmorRating;
      //rating = rating + Attributes[AttributeNames.Dexterity].Modifier;
      rating = rating + AttributePoints[(int)AttributeNames.Dexterity].Modifier;

      return rating;
    }
  }

  [BsonIgnoreExtraElements]
  public class NonPlayerCharacter : Character
  {
  }

  [BsonIgnoreExtraElements]
  public class PlayerCharacter : Character
  {
    public PlayerCharacter()
    {
      ExperiencePoints = 0;
    }

    public CharacterClass Class { get; set; }
    public int ExperiencePoints { get; private set; }
    [JsonIgnore]
    public int Level => (ExperiencePoints / XpPerLevel) == 0 ? 1 : (ExperiencePoints / XpPerLevel);

    public bool ApplyXp(int xpAmount)
    {
      int initialLevel = Level;
      bool gainedLevel = false;

      ExperiencePoints += xpAmount;

      ExperiencePoints += xpAmount;

      gainedLevel = initialLevel != Level;
      return gainedLevel;
    }
  }
}