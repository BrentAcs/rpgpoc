﻿namespace RpgPoc.Core.Characters
{
  public enum CharacterClass
  {
    None = 0,
    Fighter = 1,
    Cleric,
    Wizard,
    Thief,
    Ranger,
  }
}