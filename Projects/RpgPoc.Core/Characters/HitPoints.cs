﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace RpgPoc.Core.Characters
{
  [DebuggerDisplay("Max: {Maximum}, Current: {Current}")]
  public class HitPoints
  {
    public HitPoints()
    {
      // NOTE: Needed for Mongo persistence.
    }

    public HitPoints(int maximum) : this(maximum, maximum)
    {
    }

    [JsonConstructor]
    public HitPoints(int maximum, int current)
    {
      Maximum = maximum;
      Current = current;
    }

    public int Maximum { get; private set; }
    public int Current { get; private set; }

    public int Damage(int value)
    {
      // TODO: handle new current <= 0
      return Current -= value;
    }

    public int Heal(int value)
    {
      Current += value;
      if (Current > Maximum)
        Current = Maximum;

      return Current;
    }
  }
}