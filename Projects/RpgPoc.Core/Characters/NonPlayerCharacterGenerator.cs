﻿using System;
using System.Collections;
using System.Collections.Generic;
using RpgPoc.Core.Utilities;

namespace RpgPoc.Core.Characters
{
  public interface INonPlayerCharacterGenerator
  {
    //IEnumerable<NonPlayerCharacter> GenerateEncounterGroup(NonPlayerCharacterTemplate npcTemplate);
    NonPlayerCharacter Generate(NonPlayerCharacterTemplate npcTemplate);
  }

  public class NonPlayerCharacterGenerator : INonPlayerCharacterGenerator
  {
    //public IEnumerable<NonPlayerCharacter> GenerateEncounterGroup(NonPlayerCharacterTemplate npcTemplate)
    //{
    //  var npcs = new List<NonPlayerCharacter>();

    //  int encounterSize = DiceRoller.Roll(npcTemplate.EncounterSize);
    //  for (int i = 0; i < encounterSize; ++i)
    //  {
    //    var npc = Generate(npcTemplate);
    //    npcs.Add(npc);
    //  }                                                     

    //  return npcs;
    //}

    public NonPlayerCharacter Generate(NonPlayerCharacterTemplate npcTemplate)
    {
      var npc = new NonPlayerCharacter();
      npc.Name = npcTemplate.Name;
      npc.AttributePoints[(int)AttributeNames.Strength].BaseValue = npcTemplate.AttributePoints[(int)AttributeNames.Strength];
      npc.AttributePoints[(int)AttributeNames.Dexterity].BaseValue = npcTemplate.AttributePoints[(int)AttributeNames.Dexterity];
      npc.AttributePoints[(int)AttributeNames.Intelligence].BaseValue = npcTemplate.AttributePoints[(int)AttributeNames.Intelligence];
      npc.AttributePoints[(int)AttributeNames.Wisdom].BaseValue = npcTemplate.AttributePoints[(int)AttributeNames.Wisdom];
      npc.AttributePoints[(int)AttributeNames.Charisma].BaseValue = npcTemplate.AttributePoints[(int)AttributeNames.Charisma];
      npc.AttributePoints[(int)AttributeNames.Constitution].BaseValue = npcTemplate.AttributePoints[(int)AttributeNames.Constitution];

      int hitPoints = DiceRoller.Roll(npcTemplate.HitDice);
      npc.HitPoints = new HitPoints(hitPoints);

      return npc;
    }
  }
}