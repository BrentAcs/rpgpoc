﻿using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using MongoDB.Bson.Serialization.Attributes;
using RpgPoc.Core.Utilities;

namespace RpgPoc.Core.Characters
{
  [BsonIgnoreExtraElements]
  public class NonPlayerCharacterTemplate
  {
    public NonPlayerCharacterTemplate(string name, int level, DiceRoll hitDice, int xpValue, DiceRoll encounterSize,
      int str, int dex, int intel, int wis, int chr, int con)
    {
      int attributeCount = Enum.GetValues(typeof(AttributeNames)).Length;

      Name = name;
      Level = level;
      HitDice = hitDice;
      XpValue = xpValue;
      EncounterSize = encounterSize;
      AttributePoints = new int[attributeCount];
      AttributePoints[(int) AttributeNames.Strength] = str;
      AttributePoints[(int) AttributeNames.Dexterity] = dex;
      AttributePoints[(int) AttributeNames.Intelligence] = intel;
      AttributePoints[(int) AttributeNames.Wisdom] = wis;
      AttributePoints[(int) AttributeNames.Charisma] = chr;
      AttributePoints[(int) AttributeNames.Constitution] = con;
    }

    public string Name { get; set; }
    public int Level { get; set; }
    public DiceRoll HitDice { get; set; }
    public int XpValue { get; set; }
    public DiceRoll EncounterSize { get; set; }
    public int[] AttributePoints;
  }
}