﻿namespace RpgPoc.Core
{
  public class ConfigKeys
  {
    public const string MongoDbConnectionString = "MongoDb:ConnectionString";
    public const string MongoDbDatabaseName = "MongoDb:DatabaseName";
  }

  public class CollectionNames
  {
    public const string PlayerCharacters = "PlayerCharacters";
    public const string NonPlayerCharacterTemplates = "NonPlayerCharacterTemplates";
  }
}