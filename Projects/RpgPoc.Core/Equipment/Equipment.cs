﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using RpgPoc.Core.Utilities;

namespace RpgPoc.Core.Equipment
{
  public abstract class Item
  {
    public string Name { get; set; }
    public int BasePrice { get; set; }
  }

  public abstract class Equipment : Item
  {
  }

  public class Armor : Equipment
  {
    public int ArmorRating { get; set; }
  }

  public class Weapon : Equipment
  {
    public DiceRoll Damage { get; set; }
  }

  /// <summary>
  /// Item listed in shop for sale
  /// </summary>
  public class ShopItem
  {
    public Item Item { get; set; } // maybe use an Id member on Item and reference that ???
    public int BuyPrice { get; set; }
    public int SellPrice { get; set; }
  }

  public class MasterEquipmentCollection
  {
    public List<Armor> Armor { get; set; }
    public List<Weapon> Weapons { get; set; }
  }

  /*
   * Armor
   *  Robe          2
   *  Leather       4
   *  Scale         6
   *  Chain         8
   *  Plate         10
   *  Shield (how to handle this?) (maybe go Small & Large)
   * Weapons
   *  Club          1d4
   *  Dagger        1d4
   *  Short Sword   1d6
   *  Mace          1d6
   *  Battle Axe    1d8
   *  Long Sword    1d8
   *  Great Sword   1d10
   *  Warhammer     1d10
   */
}