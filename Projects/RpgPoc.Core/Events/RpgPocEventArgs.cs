﻿using System;
using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Events
{
  public class RpgPocEventArgs : EventArgs
  {
  }

  public class CharacterEventArgs : RpgPocEventArgs
  {
    public CharacterEventArgs(Character character)
    {
      Character = character;
    }

    public Character Character { get; set; }
  }

  public class LevelChangeEventArgs : CharacterEventArgs
  {
    public LevelChangeEventArgs(Character character) : base(character)
    {
    }

    public int NewLevel { get; set; }

  }
}