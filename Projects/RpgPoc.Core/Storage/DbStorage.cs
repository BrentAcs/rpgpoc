﻿using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Storage
{
  public interface IDbStorage
  {
    void DropAllCollections();

    IPlayerCharacterRepo PlayerCharacterRepo { get; }
    INonPlayerCharacterTemplateRepo NonPlayerCharacterTemplateRepo { get; }
  }

  public class DbStorage : IDbStorage
  {
    private readonly string _connectionString;
    private readonly string _databaseName;
    private IPlayerCharacterRepo _playerCharacterRepo;
    private INonPlayerCharacterTemplateRepo _nonPlayerCharacterTemplateRepo;

    public DbStorage(string connectionString, string databaseName)
    {
      _connectionString = connectionString;
      _databaseName = databaseName;
    }

    public void DropAllCollections()
    {
      MongoRepository<PlayerCharacter>.DropCollection(_connectionString, _databaseName, CollectionNames.PlayerCharacters);
      MongoRepository<NonPlayerCharacterTemplate>.DropCollection(_connectionString, _databaseName, CollectionNames.NonPlayerCharacterTemplates);
    }

    public IPlayerCharacterRepo PlayerCharacterRepo
    {
      get
      {
        if (null == _playerCharacterRepo)
        {
          _playerCharacterRepo = new PlayerCharacterRepo(_connectionString, _databaseName, CollectionNames.PlayerCharacters);
        }

        return _playerCharacterRepo;
      }
    }

    public INonPlayerCharacterTemplateRepo NonPlayerCharacterTemplateRepo
    {
      get
      {
        if (null == _nonPlayerCharacterTemplateRepo)
        {
          _nonPlayerCharacterTemplateRepo = new NonPlayerCharacterTemplateRepo(_connectionString, _databaseName, CollectionNames.NonPlayerCharacterTemplates);
        }

        return _nonPlayerCharacterTemplateRepo;
      }
    }
  }
}