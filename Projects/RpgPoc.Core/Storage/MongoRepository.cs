﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace RpgPoc.Core.Storage
{
  public abstract class MongoRepository<T> : IMongoRepository<T>
  {
    private static IMongoClient _mongoClient;
    private static object _mongoLock = new Object();

    public IMongoDatabase Database { get; }
    public IMongoCollection<T> Collection { get; }

    protected MongoRepository(string connectionString,
      string databaseName,
      string collectionName)
    {
      if (_mongoClient == null)
      {
        lock (_mongoLock)
        {
          if (_mongoClient == null)
          {
            _mongoClient = new MongoClient(connectionString);
          }
        }
      }

      Database = _mongoClient.GetDatabase(databaseName);
      bool collectionExists = Database
        .ListCollectionNames()
        .ToList()
        .Any(col => collectionName.Equals(col, StringComparison.CurrentCultureIgnoreCase));
      if (!collectionExists)
      {
        Database.CreateCollection(collectionName);
        Collection = Database.GetCollection<T>(collectionName);
        InitializeCollection(Collection);
      }
      else
      {
        Collection = Database.GetCollection<T>(collectionName);
      }
    }

    public static readonly FindOptions<T, T> DefaultCaseInsensitiveFindOptions = new FindOptions<T, T>
    {
      Collation = new Collation("en_US", strength: CollationStrength.Primary)
    };

    public static void DropCollection(string connectionString,
      string databaseName,
      string collectionName)
    {
      object lockObject = new object();
      IMongoClient client;

      lock (lockObject)
      {
        client = new MongoClient(connectionString);
      }

      var db = client.GetDatabase(databaseName);
      var collections = db.ListCollectionNames().ToList();
      var exists = collections.Contains(collectionName);
      if (exists)
      {
        db.DropCollection(collectionName);
      }
    }

    public virtual void InsertOne(T item, InsertOneOptions options = null, CancellationToken token = default)
    {
      Collection.InsertOne(item, options, token);
    }

    public virtual T FindFirstOrDefaultSync(FilterDefinition<T> filter, FindOptions<T, T> options = null, CancellationToken token = default)
    {
      return Collection.FindAsync(filter, options, token)
        .Result
        .FirstOrDefault();
    }

    public void Dispose()
    {
      // nothing unmanaged to dispose of.
      // if so, see:  https://docs.microsoft.com/en-us/dotnet/standard/garbage-collection/implementing-dispose
    }

    protected virtual void InitializeCollection(IMongoCollection<T> collection)
    {
    }
  }
}