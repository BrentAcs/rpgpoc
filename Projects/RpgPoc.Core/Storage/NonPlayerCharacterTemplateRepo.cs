﻿using MongoDB.Driver;
using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Storage
{
  public interface INonPlayerCharacterTemplateRepo : IMongoRepository<NonPlayerCharacterTemplate>
  {

  }

  public class NonPlayerCharacterTemplateRepo : MongoRepository<NonPlayerCharacterTemplate>, INonPlayerCharacterTemplateRepo
  {
    public NonPlayerCharacterTemplateRepo(string connectionString, string databaseName, string collectionName)
      : base(connectionString, databaseName, collectionName)
    {
    }

    protected override void InitializeCollection(IMongoCollection<NonPlayerCharacterTemplate> collection)
    {
      base.InitializeCollection(collection);

      var options = new CreateIndexOptions();
      options.Collation = new Collation("en_US",
        false,
        new Optional<CollationCaseFirst?>(CollationCaseFirst.Off),
        new Optional<CollationStrength?>(CollationStrength.Identical));
      options.Unique = true;

      var index = new CreateIndexModel<NonPlayerCharacterTemplate>("{ Name : 1 }", options);

      collection.Indexes.CreateOne(index);
    }
  }
}