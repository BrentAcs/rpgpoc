﻿using System.Threading;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using RpgPoc.Core.Characters;

namespace RpgPoc.Core.Storage
{
  public interface IPlayerCharacterRepo : IMongoRepository<PlayerCharacter>
  {
    bool PlayerCharacterByNameExists(string name);
    PlayerCharacter FindPlayerCharacterByName(string name);
  }

  public class PlayerCharacterRepo : MongoRepository<PlayerCharacter>, IPlayerCharacterRepo
  {
    public PlayerCharacterRepo(string connectionString, string databaseName, string collectionName)
      : base(connectionString, databaseName, collectionName)
    {
    }

    protected override void InitializeCollection(IMongoCollection<PlayerCharacter> collection)
    {
      base.InitializeCollection(collection);

      var options = new CreateIndexOptions();
      options.Collation = new Collation("en_US",
        false,
        new Optional<CollationCaseFirst?>(CollationCaseFirst.Off),
        new Optional<CollationStrength?>(CollationStrength.Identical));
      options.Unique = true;

      var index = new CreateIndexModel<PlayerCharacter>("{ Name : 1 }", options);

      collection.Indexes.CreateOne(index);
    }

    public bool PlayerCharacterByNameExists(string name)
    {
      return null != FindPlayerCharacterByName(name);
    }

    public PlayerCharacter FindPlayerCharacterByName(string name)
    {
      var options = new FindOptions<PlayerCharacter, PlayerCharacter>()
      {
        Collation = new Collation("en_US", strength: CollationStrength.Primary)
      };

      var filter = Builders<PlayerCharacter>
        .Filter
        .Eq(l => l.Name, name);

      var foundPc = FindFirstOrDefaultSync(filter, options);

      return foundPc;
    }
  }
}