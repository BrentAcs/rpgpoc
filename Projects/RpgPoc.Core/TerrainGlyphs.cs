﻿using System.Drawing;
using System.IO;
using System.Reflection;

namespace RpgPoc.Core
{
  public interface ITerrainGlyphs
  {
    Size GlyphSize { get; }

    Bitmap Get(TerrainTypes terrain, int stage = 0);
  }

  public class TerrainGlyphs : ITerrainGlyphs
  {
    private Bitmap _glyphs;

    public TerrainGlyphs()
    {
      Assembly ass = Assembly.GetExecutingAssembly();
      using (Stream stream = ass.GetManifestResourceStream(ResourceNames.TerrainGlyphs))
        _glyphs = new Bitmap(stream);
    }
    
    public Size GlyphSize => new Size(_glyphs.Height, _glyphs.Height);

    public Bitmap Get(TerrainTypes terrain, int stage = 0)
    {
      Bitmap result = new Bitmap(GlyphSize.Width, GlyphSize.Height);

      using(Graphics g = Graphics.FromImage(_glyphs))
        g.DrawImage( result, new Point(0,0));
        //g.DrawImage(result, new Point(0,0), new Rectangle(0,0,GlyphSize.Width, GlyphSize.Height));

      return result;
    }
  }
}