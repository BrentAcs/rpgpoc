﻿namespace RpgPoc.Core
{
  public enum TerrainTypes
  {
    None = 0,

    WaterDeep = 1,     // passable only w/ large ships
    WaterShallow,  // passable w/ any ship
    WaterRiver,    // maybe not needed

    Plains,
    Forest,
    Hills,         // slows travel?
    Mountains,     // impassible
    Desert,
  }
}
