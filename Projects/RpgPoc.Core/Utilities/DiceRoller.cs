﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace RpgPoc.Core.Utilities
{
  public enum Dice
  {
    D4,
    D6,
    D8,
    D10,
    D12,
    D20,
    D100,
  }

  public class DiceRoll
  {
    public DiceRoll(Dice dice, int count=1, int modifier=0)
    {
      Dice = dice;
      Count = count;
      Modifier = modifier;
    }

    public Dice Dice { get; set; }
    public int Count { get; set; }
    public int Modifier { get; set; }
  }

  public static class DiceRoller
  {
    public static int Roll(DiceRoll diceRoll)
    {
      int result = 0;

      for (int i = 0; i < diceRoll.Count; i++)
      {
        switch (diceRoll.Dice)
        {
          case Dice.D4:
            result += Rnd.Next(0, 4) + 1; break;
          case Dice.D6:
            result += Rnd.Next(0, 6) + 1; break;
          case Dice.D8:
            result += Rnd.Next(0, 8) + 1; break;
          case Dice.D10:
            result += Rnd.Next(0, 10) + 1; break;
          case Dice.D12:
            result += Rnd.Next(0, 12) + 1; break;
          case Dice.D20:
            result += Rnd.Next(0, 20) + 1; break;
          case Dice.D100:
            result += Rnd.Next(0, 99) + 1; break;
        }
      }

      result += diceRoll.Modifier;

      return result;
    }
  }
}