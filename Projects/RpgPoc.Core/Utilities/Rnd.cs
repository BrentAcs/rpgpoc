﻿using System;

namespace RpgPoc.Core.Utilities
{
  public static class Rnd
  {
    private static readonly Random _random = new Random();
    private static readonly object _syncLock = new object();

    public static int Next(int min, int max)
    {
      lock (_syncLock)
      {
        return _random.Next(min, max);
      }
    }

    public static int Next(int max)
    {
      lock (_syncLock)
      {
        return _random.Next(max);
      }
    }
  }
}