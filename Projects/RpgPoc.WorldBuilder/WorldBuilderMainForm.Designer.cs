﻿namespace RpgPoc.WorldBuilder
{
  partial class WorldBuilderMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WorldBuilderMainForm));
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.testToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.testPictureBox = new System.Windows.Forms.PictureBox();
      this.menuStrip1.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.testPictureBox)).BeginInit();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
      this.menuStrip1.Location = new System.Drawing.Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new System.Drawing.Size(871, 24);
      this.menuStrip1.TabIndex = 0;
      this.menuStrip1.Text = "mainMenuStrip";
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
      this.fileToolStripMenuItem.Text = "&File";
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripButton});
      this.toolStrip1.Location = new System.Drawing.Point(0, 24);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(871, 25);
      this.toolStrip1.TabIndex = 1;
      this.toolStrip1.Text = "mainToolStrip";
      // 
      // testToolStripButton
      // 
      this.testToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.testToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("testToolStripButton.Image")));
      this.testToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.testToolStripButton.Name = "testToolStripButton";
      this.testToolStripButton.Size = new System.Drawing.Size(31, 22);
      this.testToolStripButton.Text = "Test";
      this.testToolStripButton.Click += new System.EventHandler(this.testToolStripButton_Click);
      // 
      // statusStrip1
      // 
      this.statusStrip1.Location = new System.Drawing.Point(0, 616);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(871, 22);
      this.statusStrip1.TabIndex = 2;
      this.statusStrip1.Text = "mainStatusStrip";
      // 
      // testPictureBox
      // 
      this.testPictureBox.Location = new System.Drawing.Point(12, 65);
      this.testPictureBox.Name = "testPictureBox";
      this.testPictureBox.Size = new System.Drawing.Size(301, 226);
      this.testPictureBox.TabIndex = 3;
      this.testPictureBox.TabStop = false;
      // 
      // WorldBuilderMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(871, 638);
      this.Controls.Add(this.testPictureBox);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.toolStrip1);
      this.Controls.Add(this.menuStrip1);
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "WorldBuilderMainForm";
      this.Text = "World Builder";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WorldBuilderMainForm_FormClosing);
      this.Load += new System.EventHandler(this.WorldBuilderMainForm_Load);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.testPictureBox)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripButton testToolStripButton;
    private System.Windows.Forms.PictureBox testPictureBox;
  }
}

