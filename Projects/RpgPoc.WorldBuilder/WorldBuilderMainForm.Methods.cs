﻿using System.Windows.Forms;

namespace RpgPoc.WorldBuilder
{
  public partial class WorldBuilderMainForm : Form
  {
    private void LoadSettings()
    {
      Location = Properties.Settings.Default.MainFormLocation;
      Size = Properties.Settings.Default.MainFormSize;
    }

    private void SaveSettings()
    {
      Properties.Settings.Default.MainFormLocation = Location;
      Properties.Settings.Default.MainFormSize = Size;

      Properties.Settings.Default.Save();
    }
  }
}