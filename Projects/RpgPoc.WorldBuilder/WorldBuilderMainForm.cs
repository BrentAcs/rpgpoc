﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RpgPoc.Core;

namespace RpgPoc.WorldBuilder
{
  public partial class WorldBuilderMainForm : Form
  {
    public WorldBuilderMainForm()
    {
      InitializeComponent();
    }

    private void WorldBuilderMainForm_Load(object sender, EventArgs e)
    {
      LoadSettings();
    }

    private void WorldBuilderMainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      SaveSettings();
    }

    private void testToolStripButton_Click(object sender, EventArgs e)
    {
      try
      {
        testPictureBox.Image = new Bitmap(500,500);

        ITerrainGlyphs terrainGlyphs = new TerrainGlyphs();
        var glyph1 = terrainGlyphs.Get(TerrainTypes.None);

        using(Graphics g = Graphics.FromImage(testPictureBox.Image))
          g.DrawImage(glyph1, new Point(0,0));



      }
      catch (Exception ex)
      {
        Trace.WriteLine(ex);
      }
    }
  }
}
