﻿namespace RpgPoc.Controls
{
  partial class PartyListView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.theListView = new System.Windows.Forms.ListView();
      this.nameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.strengthColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.dexterifyColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.intelligenceColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.wisdomColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.charismaColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.constitutionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.levelColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.hitPointsCurrentColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.hitPointsMaximumColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.armorRatingColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.classColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.SuspendLayout();
      // 
      // theListView
      // 
      this.theListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumnHeader,
            this.strengthColumnHeader,
            this.dexterifyColumnHeader,
            this.intelligenceColumnHeader,
            this.wisdomColumnHeader,
            this.charismaColumnHeader,
            this.constitutionColumnHeader,
            this.levelColumnHeader,
            this.hitPointsCurrentColumnHeader,
            this.hitPointsMaximumColumnHeader,
            this.armorRatingColumnHeader,
            this.classColumnHeader});
      this.theListView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.theListView.HideSelection = false;
      this.theListView.Location = new System.Drawing.Point(0, 0);
      this.theListView.Name = "theListView";
      this.theListView.Size = new System.Drawing.Size(890, 313);
      this.theListView.TabIndex = 0;
      this.theListView.UseCompatibleStateImageBehavior = false;
      this.theListView.View = System.Windows.Forms.View.Details;
      // 
      // nameColumnHeader
      // 
      this.nameColumnHeader.Text = "Name";
      // 
      // strengthColumnHeader
      // 
      this.strengthColumnHeader.Text = "Str";
      this.strengthColumnHeader.Width = 32;
      // 
      // dexterifyColumnHeader
      // 
      this.dexterifyColumnHeader.Text = "Dex";
      this.dexterifyColumnHeader.Width = 32;
      // 
      // intelligenceColumnHeader
      // 
      this.intelligenceColumnHeader.Text = "Int";
      this.intelligenceColumnHeader.Width = 32;
      // 
      // wisdomColumnHeader
      // 
      this.wisdomColumnHeader.Text = "Wis";
      this.wisdomColumnHeader.Width = 32;
      // 
      // charismaColumnHeader
      // 
      this.charismaColumnHeader.Text = "Chr";
      this.charismaColumnHeader.Width = 32;
      // 
      // constitutionColumnHeader
      // 
      this.constitutionColumnHeader.Text = "Con";
      this.constitutionColumnHeader.Width = 32;
      // 
      // levelColumnHeader
      // 
      this.levelColumnHeader.Text = "Level";
      this.levelColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.levelColumnHeader.Width = 64;
      // 
      // hitPointsCurrentColumnHeader
      // 
      this.hitPointsCurrentColumnHeader.Text = "Hit Points";
      this.hitPointsCurrentColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.hitPointsCurrentColumnHeader.Width = 64;
      // 
      // hitPointsMaximumColumnHeader
      // 
      this.hitPointsMaximumColumnHeader.Text = "Max HP";
      this.hitPointsMaximumColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.hitPointsMaximumColumnHeader.Width = 64;
      // 
      // armorRatingColumnHeader
      // 
      this.armorRatingColumnHeader.Text = "Armor";
      this.armorRatingColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.armorRatingColumnHeader.Width = 64;
      // 
      // classColumnHeader
      // 
      this.classColumnHeader.Text = "Class";
      this.classColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.classColumnHeader.Width = 96;
      // 
      // PartyListView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.theListView);
      this.Name = "PartyListView";
      this.Size = new System.Drawing.Size(890, 313);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListView theListView;
    private System.Windows.Forms.ColumnHeader nameColumnHeader;
    private System.Windows.Forms.ColumnHeader strengthColumnHeader;
    private System.Windows.Forms.ColumnHeader dexterifyColumnHeader;
    private System.Windows.Forms.ColumnHeader intelligenceColumnHeader;
    private System.Windows.Forms.ColumnHeader wisdomColumnHeader;
    private System.Windows.Forms.ColumnHeader charismaColumnHeader;
    private System.Windows.Forms.ColumnHeader constitutionColumnHeader;
    private System.Windows.Forms.ColumnHeader levelColumnHeader;
    private System.Windows.Forms.ColumnHeader hitPointsCurrentColumnHeader;
    private System.Windows.Forms.ColumnHeader hitPointsMaximumColumnHeader;
    private System.Windows.Forms.ColumnHeader armorRatingColumnHeader;
    private System.Windows.Forms.ColumnHeader classColumnHeader;
  }
}
