﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RpgPoc.Core.Characters;

namespace RpgPoc.Controls
{
  public partial class PartyListView : UserControl
  {
    public PartyListView()
    {
      InitializeComponent();
    }

    public void LoadSettings()
    {
      theListView.Columns[0].Width = Properties.Settings.Default.PartyListView_NameColumnHeader_Width;
      theListView.Columns[7].Width = Properties.Settings.Default.PartyListView_LevelColumnHeader_Width;
      theListView.Columns[8].Width = Properties.Settings.Default.PartyListView_CurrentHitPointsColumnHeader_Width;
      theListView.Columns[9].Width = Properties.Settings.Default.PartyListView_MaximumHitPointsColumnHeader_Width;
      theListView.Columns[10].Width = Properties.Settings.Default.PartyListView_ArmorClassColumnHeader_Width;
      theListView.Columns[11].Width = Properties.Settings.Default.PartyListView_ClassColumnHeader_Width;
    }

    public void SaveSettings()
    {
      Properties.Settings.Default.PartyListView_NameColumnHeader_Width = theListView.Columns[0].Width;
      Properties.Settings.Default.PartyListView_LevelColumnHeader_Width = theListView.Columns[7].Width;
      Properties.Settings.Default.PartyListView_CurrentHitPointsColumnHeader_Width = theListView.Columns[8].Width;
      Properties.Settings.Default.PartyListView_MaximumHitPointsColumnHeader_Width = theListView.Columns[9].Width;
      Properties.Settings.Default.PartyListView_ArmorClassColumnHeader_Width = theListView.Columns[10].Width;
      Properties.Settings.Default.PartyListView_ClassColumnHeader_Width = theListView.Columns[11].Width;
    }

    public void AddPlayerCharacter(PlayerCharacter playerCharacter)
    {
      var item = theListView.Items.Add(playerCharacter.Name);
      item.Tag = playerCharacter;
      item.SubItems.Add(playerCharacter.AttributePoints[(int)AttributeNames.Strength].BaseValue.ToString());
      item.SubItems.Add(playerCharacter.AttributePoints[(int)AttributeNames.Dexterity].BaseValue.ToString());
      item.SubItems.Add(playerCharacter.AttributePoints[(int)AttributeNames.Intelligence].BaseValue.ToString());
      item.SubItems.Add(playerCharacter.AttributePoints[(int)AttributeNames.Wisdom].BaseValue.ToString());
      item.SubItems.Add(playerCharacter.AttributePoints[(int)AttributeNames.Charisma].BaseValue.ToString());
      item.SubItems.Add(playerCharacter.AttributePoints[(int)AttributeNames.Constitution].BaseValue.ToString());
      item.SubItems.Add(playerCharacter.Level.ToString());
      item.SubItems.Add(playerCharacter.HitPoints.Current.ToString());
      item.SubItems.Add(playerCharacter.HitPoints.Maximum.ToString());
      item.SubItems.Add(playerCharacter.ArmorRating.ToString());
      item.SubItems.Add(playerCharacter.Class.ToString());
    }
  }
}
