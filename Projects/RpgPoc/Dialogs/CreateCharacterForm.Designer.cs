﻿namespace RpgPoc.Dialogs
{
  partial class CreateCharacterForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.contentPanel = new System.Windows.Forms.Panel();
      this.armorRatingTextBox = new System.Windows.Forms.TextBox();
      this.hitPointsTextBox = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.availablePointsTextBox = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.classComboBox = new System.Windows.Forms.ComboBox();
      this.label8 = new System.Windows.Forms.Label();
      this.nameTextBox = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.attributesGroupBox = new System.Windows.Forms.GroupBox();
      this.constitutionMinusButton = new System.Windows.Forms.Button();
      this.constitutionPlusButton = new System.Windows.Forms.Button();
      this.constitutionTextBox = new System.Windows.Forms.TextBox();
      this.charismaMinusButton = new System.Windows.Forms.Button();
      this.charismaPlusButton = new System.Windows.Forms.Button();
      this.charismaTextBox = new System.Windows.Forms.TextBox();
      this.wisdomMinusButton = new System.Windows.Forms.Button();
      this.wisdomPlusButton = new System.Windows.Forms.Button();
      this.wisdomTextBox = new System.Windows.Forms.TextBox();
      this.intelligenceMinusButton = new System.Windows.Forms.Button();
      this.intelligencePlusButton = new System.Windows.Forms.Button();
      this.intelligenceTextBox = new System.Windows.Forms.TextBox();
      this.dexterityMinusButton = new System.Windows.Forms.Button();
      this.dexterityPlusButton = new System.Windows.Forms.Button();
      this.dexterityTextBox = new System.Windows.Forms.TextBox();
      this.strengthMinusButton = new System.Windows.Forms.Button();
      this.strengthPlusButton = new System.Windows.Forms.Button();
      this.strengthTextBox = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.theOkButton = new System.Windows.Forms.Button();
      this.theCancelButton = new System.Windows.Forms.Button();
      this.theErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
      this.contentPanel.SuspendLayout();
      this.attributesGroupBox.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.theErrorProvider)).BeginInit();
      this.SuspendLayout();
      // 
      // contentPanel
      // 
      this.contentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.contentPanel.Controls.Add(this.armorRatingTextBox);
      this.contentPanel.Controls.Add(this.hitPointsTextBox);
      this.contentPanel.Controls.Add(this.label11);
      this.contentPanel.Controls.Add(this.label10);
      this.contentPanel.Controls.Add(this.availablePointsTextBox);
      this.contentPanel.Controls.Add(this.label9);
      this.contentPanel.Controls.Add(this.classComboBox);
      this.contentPanel.Controls.Add(this.label8);
      this.contentPanel.Controls.Add(this.nameTextBox);
      this.contentPanel.Controls.Add(this.label1);
      this.contentPanel.Location = new System.Drawing.Point(12, 12);
      this.contentPanel.Name = "contentPanel";
      this.contentPanel.Size = new System.Drawing.Size(563, 328);
      this.contentPanel.TabIndex = 0;
      // 
      // armorRatingTextBox
      // 
      this.armorRatingTextBox.Location = new System.Drawing.Point(299, 90);
      this.armorRatingTextBox.Name = "armorRatingTextBox";
      this.armorRatingTextBox.ReadOnly = true;
      this.armorRatingTextBox.Size = new System.Drawing.Size(48, 20);
      this.armorRatingTextBox.TabIndex = 9;
      // 
      // hitPointsTextBox
      // 
      this.hitPointsTextBox.Location = new System.Drawing.Point(299, 64);
      this.hitPointsTextBox.Name = "hitPointsTextBox";
      this.hitPointsTextBox.ReadOnly = true;
      this.hitPointsTextBox.Size = new System.Drawing.Size(48, 20);
      this.hitPointsTextBox.TabIndex = 8;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(225, 94);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(68, 13);
      this.label11.TabIndex = 7;
      this.label11.Text = "Armor Rating";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(223, 67);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(52, 13);
      this.label10.TabIndex = 6;
      this.label10.Text = "Hit Points";
      // 
      // availablePointsTextBox
      // 
      this.availablePointsTextBox.Location = new System.Drawing.Point(131, 228);
      this.availablePointsTextBox.Name = "availablePointsTextBox";
      this.availablePointsTextBox.ReadOnly = true;
      this.availablePointsTextBox.Size = new System.Drawing.Size(70, 20);
      this.availablePointsTextBox.TabIndex = 5;
      this.availablePointsTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.availablePointsTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.availablePointsTextBox_Validating);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(10, 231);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(82, 13);
      this.label9.TabIndex = 4;
      this.label9.Text = "Available Points";
      // 
      // classComboBox
      // 
      this.classComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.classComboBox.FormattingEnabled = true;
      this.classComboBox.Location = new System.Drawing.Point(261, 14);
      this.classComboBox.Name = "classComboBox";
      this.classComboBox.Size = new System.Drawing.Size(121, 21);
      this.classComboBox.TabIndex = 3;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(223, 17);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(32, 13);
      this.label8.TabIndex = 2;
      this.label8.Text = "Class";
      // 
      // nameTextBox
      // 
      this.nameTextBox.Location = new System.Drawing.Point(77, 14);
      this.nameTextBox.Name = "nameTextBox";
      this.nameTextBox.Size = new System.Drawing.Size(124, 20);
      this.nameTextBox.TabIndex = 1;
      this.nameTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.nameTextBox_Validating);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(7, 17);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Name";
      // 
      // attributesGroupBox
      // 
      this.attributesGroupBox.Controls.Add(this.constitutionMinusButton);
      this.attributesGroupBox.Controls.Add(this.constitutionPlusButton);
      this.attributesGroupBox.Controls.Add(this.constitutionTextBox);
      this.attributesGroupBox.Controls.Add(this.charismaMinusButton);
      this.attributesGroupBox.Controls.Add(this.charismaPlusButton);
      this.attributesGroupBox.Controls.Add(this.charismaTextBox);
      this.attributesGroupBox.Controls.Add(this.wisdomMinusButton);
      this.attributesGroupBox.Controls.Add(this.wisdomPlusButton);
      this.attributesGroupBox.Controls.Add(this.wisdomTextBox);
      this.attributesGroupBox.Controls.Add(this.intelligenceMinusButton);
      this.attributesGroupBox.Controls.Add(this.intelligencePlusButton);
      this.attributesGroupBox.Controls.Add(this.intelligenceTextBox);
      this.attributesGroupBox.Controls.Add(this.dexterityMinusButton);
      this.attributesGroupBox.Controls.Add(this.dexterityPlusButton);
      this.attributesGroupBox.Controls.Add(this.dexterityTextBox);
      this.attributesGroupBox.Controls.Add(this.strengthMinusButton);
      this.attributesGroupBox.Controls.Add(this.strengthPlusButton);
      this.attributesGroupBox.Controls.Add(this.strengthTextBox);
      this.attributesGroupBox.Controls.Add(this.label2);
      this.attributesGroupBox.Controls.Add(this.label7);
      this.attributesGroupBox.Controls.Add(this.label3);
      this.attributesGroupBox.Controls.Add(this.label6);
      this.attributesGroupBox.Controls.Add(this.label4);
      this.attributesGroupBox.Controls.Add(this.label5);
      this.attributesGroupBox.Location = new System.Drawing.Point(12, 52);
      this.attributesGroupBox.Name = "attributesGroupBox";
      this.attributesGroupBox.Size = new System.Drawing.Size(219, 182);
      this.attributesGroupBox.TabIndex = 8;
      this.attributesGroupBox.TabStop = false;
      this.attributesGroupBox.Text = "Attributes";
      // 
      // constitutionMinusButton
      // 
      this.constitutionMinusButton.Location = new System.Drawing.Point(169, 154);
      this.constitutionMinusButton.Name = "constitutionMinusButton";
      this.constitutionMinusButton.Size = new System.Drawing.Size(32, 20);
      this.constitutionMinusButton.TabIndex = 25;
      this.constitutionMinusButton.Text = "-";
      this.constitutionMinusButton.UseVisualStyleBackColor = true;
      this.constitutionMinusButton.Click += new System.EventHandler(this.constitutionMinusButton_Click);
      // 
      // constitutionPlusButton
      // 
      this.constitutionPlusButton.Location = new System.Drawing.Point(131, 155);
      this.constitutionPlusButton.Name = "constitutionPlusButton";
      this.constitutionPlusButton.Size = new System.Drawing.Size(32, 20);
      this.constitutionPlusButton.TabIndex = 24;
      this.constitutionPlusButton.Text = "+";
      this.constitutionPlusButton.UseVisualStyleBackColor = true;
      this.constitutionPlusButton.Click += new System.EventHandler(this.constitutionPlusButton_Click);
      // 
      // constitutionTextBox
      // 
      this.constitutionTextBox.Enabled = false;
      this.constitutionTextBox.Location = new System.Drawing.Point(77, 155);
      this.constitutionTextBox.Name = "constitutionTextBox";
      this.constitutionTextBox.ReadOnly = true;
      this.constitutionTextBox.Size = new System.Drawing.Size(48, 20);
      this.constitutionTextBox.TabIndex = 23;
      // 
      // charismaMinusButton
      // 
      this.charismaMinusButton.Location = new System.Drawing.Point(169, 128);
      this.charismaMinusButton.Name = "charismaMinusButton";
      this.charismaMinusButton.Size = new System.Drawing.Size(32, 20);
      this.charismaMinusButton.TabIndex = 22;
      this.charismaMinusButton.Text = "-";
      this.charismaMinusButton.UseVisualStyleBackColor = true;
      this.charismaMinusButton.Click += new System.EventHandler(this.charismaMinusButton_Click);
      // 
      // charismaPlusButton
      // 
      this.charismaPlusButton.Location = new System.Drawing.Point(131, 129);
      this.charismaPlusButton.Name = "charismaPlusButton";
      this.charismaPlusButton.Size = new System.Drawing.Size(32, 20);
      this.charismaPlusButton.TabIndex = 21;
      this.charismaPlusButton.Text = "+";
      this.charismaPlusButton.UseVisualStyleBackColor = true;
      this.charismaPlusButton.Click += new System.EventHandler(this.charismaPlusButton_Click);
      // 
      // charismaTextBox
      // 
      this.charismaTextBox.Enabled = false;
      this.charismaTextBox.Location = new System.Drawing.Point(77, 129);
      this.charismaTextBox.Name = "charismaTextBox";
      this.charismaTextBox.ReadOnly = true;
      this.charismaTextBox.Size = new System.Drawing.Size(48, 20);
      this.charismaTextBox.TabIndex = 20;
      // 
      // wisdomMinusButton
      // 
      this.wisdomMinusButton.Location = new System.Drawing.Point(169, 102);
      this.wisdomMinusButton.Name = "wisdomMinusButton";
      this.wisdomMinusButton.Size = new System.Drawing.Size(32, 20);
      this.wisdomMinusButton.TabIndex = 19;
      this.wisdomMinusButton.Text = "-";
      this.wisdomMinusButton.UseVisualStyleBackColor = true;
      this.wisdomMinusButton.Click += new System.EventHandler(this.wisdomMinusButton_Click);
      // 
      // wisdomPlusButton
      // 
      this.wisdomPlusButton.Location = new System.Drawing.Point(131, 103);
      this.wisdomPlusButton.Name = "wisdomPlusButton";
      this.wisdomPlusButton.Size = new System.Drawing.Size(32, 20);
      this.wisdomPlusButton.TabIndex = 18;
      this.wisdomPlusButton.Text = "+";
      this.wisdomPlusButton.UseVisualStyleBackColor = true;
      this.wisdomPlusButton.Click += new System.EventHandler(this.wisdomPlusButton_Click);
      // 
      // wisdomTextBox
      // 
      this.wisdomTextBox.Enabled = false;
      this.wisdomTextBox.Location = new System.Drawing.Point(77, 103);
      this.wisdomTextBox.Name = "wisdomTextBox";
      this.wisdomTextBox.ReadOnly = true;
      this.wisdomTextBox.Size = new System.Drawing.Size(48, 20);
      this.wisdomTextBox.TabIndex = 17;
      // 
      // intelligenceMinusButton
      // 
      this.intelligenceMinusButton.Location = new System.Drawing.Point(169, 76);
      this.intelligenceMinusButton.Name = "intelligenceMinusButton";
      this.intelligenceMinusButton.Size = new System.Drawing.Size(32, 20);
      this.intelligenceMinusButton.TabIndex = 16;
      this.intelligenceMinusButton.Text = "-";
      this.intelligenceMinusButton.UseVisualStyleBackColor = true;
      this.intelligenceMinusButton.Click += new System.EventHandler(this.intelligenceMinusButton_Click);
      // 
      // intelligencePlusButton
      // 
      this.intelligencePlusButton.Location = new System.Drawing.Point(131, 77);
      this.intelligencePlusButton.Name = "intelligencePlusButton";
      this.intelligencePlusButton.Size = new System.Drawing.Size(32, 20);
      this.intelligencePlusButton.TabIndex = 15;
      this.intelligencePlusButton.Text = "+";
      this.intelligencePlusButton.UseVisualStyleBackColor = true;
      this.intelligencePlusButton.Click += new System.EventHandler(this.intelligencePlusButton_Click);
      // 
      // intelligenceTextBox
      // 
      this.intelligenceTextBox.Enabled = false;
      this.intelligenceTextBox.Location = new System.Drawing.Point(77, 77);
      this.intelligenceTextBox.Name = "intelligenceTextBox";
      this.intelligenceTextBox.ReadOnly = true;
      this.intelligenceTextBox.Size = new System.Drawing.Size(48, 20);
      this.intelligenceTextBox.TabIndex = 14;
      // 
      // dexterityMinusButton
      // 
      this.dexterityMinusButton.Location = new System.Drawing.Point(169, 50);
      this.dexterityMinusButton.Name = "dexterityMinusButton";
      this.dexterityMinusButton.Size = new System.Drawing.Size(32, 20);
      this.dexterityMinusButton.TabIndex = 13;
      this.dexterityMinusButton.Text = "-";
      this.dexterityMinusButton.UseVisualStyleBackColor = true;
      this.dexterityMinusButton.Click += new System.EventHandler(this.dexterityMinusButton_Click);
      // 
      // dexterityPlusButton
      // 
      this.dexterityPlusButton.Location = new System.Drawing.Point(131, 51);
      this.dexterityPlusButton.Name = "dexterityPlusButton";
      this.dexterityPlusButton.Size = new System.Drawing.Size(32, 20);
      this.dexterityPlusButton.TabIndex = 12;
      this.dexterityPlusButton.Text = "+";
      this.dexterityPlusButton.UseVisualStyleBackColor = true;
      this.dexterityPlusButton.Click += new System.EventHandler(this.dexterityPlusButton_Click);
      // 
      // dexterityTextBox
      // 
      this.dexterityTextBox.Enabled = false;
      this.dexterityTextBox.Location = new System.Drawing.Point(77, 51);
      this.dexterityTextBox.Name = "dexterityTextBox";
      this.dexterityTextBox.ReadOnly = true;
      this.dexterityTextBox.Size = new System.Drawing.Size(48, 20);
      this.dexterityTextBox.TabIndex = 11;
      // 
      // strengthMinusButton
      // 
      this.strengthMinusButton.Location = new System.Drawing.Point(169, 24);
      this.strengthMinusButton.Name = "strengthMinusButton";
      this.strengthMinusButton.Size = new System.Drawing.Size(32, 20);
      this.strengthMinusButton.TabIndex = 10;
      this.strengthMinusButton.Text = "-";
      this.strengthMinusButton.UseVisualStyleBackColor = true;
      this.strengthMinusButton.Click += new System.EventHandler(this.strengthMinusButton_Click);
      // 
      // strengthPlusButton
      // 
      this.strengthPlusButton.Location = new System.Drawing.Point(131, 25);
      this.strengthPlusButton.Name = "strengthPlusButton";
      this.strengthPlusButton.Size = new System.Drawing.Size(32, 20);
      this.strengthPlusButton.TabIndex = 9;
      this.strengthPlusButton.Text = "+";
      this.strengthPlusButton.UseVisualStyleBackColor = true;
      this.strengthPlusButton.Click += new System.EventHandler(this.strengthPlusButton_Click);
      // 
      // strengthTextBox
      // 
      this.strengthTextBox.Enabled = false;
      this.strengthTextBox.Location = new System.Drawing.Point(77, 25);
      this.strengthTextBox.Name = "strengthTextBox";
      this.strengthTextBox.ReadOnly = true;
      this.strengthTextBox.Size = new System.Drawing.Size(48, 20);
      this.strengthTextBox.TabIndex = 8;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(7, 27);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(47, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Strength";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(10, 158);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(62, 13);
      this.label7.TabIndex = 7;
      this.label7.Text = "Constitution";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(10, 53);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(48, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "Dexterity";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(10, 132);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(50, 13);
      this.label6.TabIndex = 6;
      this.label6.Text = "Charisma";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(10, 80);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(61, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "Intelligence";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(10, 105);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(45, 13);
      this.label5.TabIndex = 5;
      this.label5.Text = "Wisdom";
      // 
      // theOkButton
      // 
      this.theOkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.theOkButton.Location = new System.Drawing.Point(419, 346);
      this.theOkButton.Name = "theOkButton";
      this.theOkButton.Size = new System.Drawing.Size(75, 23);
      this.theOkButton.TabIndex = 9;
      this.theOkButton.Text = "OK";
      this.theOkButton.UseVisualStyleBackColor = true;
      this.theOkButton.Click += new System.EventHandler(this.theOkButton_Click);
      // 
      // theCancelButton
      // 
      this.theCancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.theCancelButton.CausesValidation = false;
      this.theCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.theCancelButton.Location = new System.Drawing.Point(500, 346);
      this.theCancelButton.Name = "theCancelButton";
      this.theCancelButton.Size = new System.Drawing.Size(75, 23);
      this.theCancelButton.TabIndex = 10;
      this.theCancelButton.Text = "Cancel";
      this.theCancelButton.UseVisualStyleBackColor = true;
      // 
      // theErrorProvider
      // 
      this.theErrorProvider.ContainerControl = this;
      // 
      // CreateCharacterForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(587, 381);
      this.Controls.Add(this.theCancelButton);
      this.Controls.Add(this.theOkButton);
      this.Controls.Add(this.attributesGroupBox);
      this.Controls.Add(this.contentPanel);
      this.Name = "CreateCharacterForm";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Create Character";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateCharacterForm_FormClosing);
      this.Load += new System.EventHandler(this.CreateCharacterForm_Load);
      this.contentPanel.ResumeLayout(false);
      this.contentPanel.PerformLayout();
      this.attributesGroupBox.ResumeLayout(false);
      this.attributesGroupBox.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.theErrorProvider)).EndInit();
      this.ResumeLayout(false);

    }

        #endregion

        private System.Windows.Forms.Panel contentPanel;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox attributesGroupBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button constitutionMinusButton;
        private System.Windows.Forms.Button constitutionPlusButton;
        private System.Windows.Forms.TextBox constitutionTextBox;
        private System.Windows.Forms.Button charismaMinusButton;
        private System.Windows.Forms.Button charismaPlusButton;
        private System.Windows.Forms.TextBox charismaTextBox;
        private System.Windows.Forms.Button wisdomMinusButton;
        private System.Windows.Forms.Button wisdomPlusButton;
        private System.Windows.Forms.TextBox wisdomTextBox;
        private System.Windows.Forms.Button intelligenceMinusButton;
        private System.Windows.Forms.Button intelligencePlusButton;
        private System.Windows.Forms.TextBox intelligenceTextBox;
        private System.Windows.Forms.Button dexterityMinusButton;
        private System.Windows.Forms.Button dexterityPlusButton;
        private System.Windows.Forms.TextBox dexterityTextBox;
        private System.Windows.Forms.Button strengthMinusButton;
        private System.Windows.Forms.Button strengthPlusButton;
        private System.Windows.Forms.TextBox strengthTextBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox classComboBox;
        private System.Windows.Forms.TextBox availablePointsTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button theOkButton;
        private System.Windows.Forms.Button theCancelButton;
    private System.Windows.Forms.TextBox hitPointsTextBox;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox armorRatingTextBox;
        private System.Windows.Forms.ErrorProvider theErrorProvider;
    }
}