﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Serializers;
using RpgPoc.Core.Characters;

namespace RpgPoc.Dialogs
{
  public partial class CreateCharacterForm : Form
  {
    private const int MaximumAvailablePoints = 40;
    private const string UnnamedName = "(unnamed)";

    private List<TextBox> _pointTextBoxes;
    private PlayerCharacter _playerCharacter;

    public CreateCharacterForm()
    {
      InitializeComponent();

      string[] names = Enum.GetNames(typeof(CharacterClass));
      classComboBox.Items.AddRange(names);
      classComboBox.Items.RemoveAt(0);
      classComboBox.SelectedIndex = 0;

      _pointTextBoxes = new List<TextBox>
      {
        strengthTextBox,
        dexterityTextBox,
        intelligenceTextBox,
        wisdomTextBox,
        charismaTextBox,
        constitutionTextBox,
      };

      AvailablePoints = MaximumAvailablePoints;

      _playerCharacter = new PlayerCharacter
      {
        Name = UnnamedName,
        Class = CharacterClass.None,
      };
    }

    public PlayerCharacter PlayerCharacter => _playerCharacter;

    private void CreateCharacterForm_Load(object sender, EventArgs e)
    {
      nameTextBox.Text = _playerCharacter.Name;
      hitPointsTextBox.Text = _playerCharacter.HitPoints.Maximum.ToString();
      armorRatingTextBox.Text = _playerCharacter.ArmorRating.ToString();

      for (AttributeNames attributeName = AttributeNames.Strength; attributeName <= AttributeNames.Constitution; attributeName++)
      {
        _pointTextBoxes[(int)attributeName].Text = _playerCharacter.AttributePoints[(int)attributeName].BaseValue.ToString();
      }

      // NOTE: test so I don't have to click 40 times
      nameTextBox.Text = "Gotch";
      _pointTextBoxes[(int)AttributeNames.Strength].Text = "20";
      _pointTextBoxes[(int)AttributeNames.Dexterity].Text = "20";
      _pointTextBoxes[(int)AttributeNames.Intelligence].Text = "20";
      _pointTextBoxes[(int)AttributeNames.Wisdom].Text = "20";
      AvailablePoints = 0;
    }

    private void CreateCharacterForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      Trace.WriteLine($"{e.CloseReason}");
    }

    private void theOkButton_Click(object sender, EventArgs e)
    {
      bool result = ValidateChildren();
      if (result)
      {
        _playerCharacter.Name = nameTextBox.Text;

        CharacterClass characterClass;
        Enum.TryParse(classComboBox.Text, out characterClass);
        _playerCharacter.Class = characterClass;

        for (AttributeNames attributeName = AttributeNames.Strength; attributeName <= AttributeNames.Constitution; attributeName++)
        {
          _playerCharacter.AttributePoints[(int)attributeName].BaseValue = int.Parse(_pointTextBoxes[(int)attributeName].Text);
        }

        DialogResult = DialogResult.OK;
      }
    }

    private int AvailablePoints
    {
      get
      {
        int points;
        int.TryParse(availablePointsTextBox.Text, out points);

        return points;
      }
      set => availablePointsTextBox.Text = value.ToString();
    }

    private void IncrementAttribute(TextBox textBox)
    {
      int currentValue = 0;
      int.TryParse(textBox.Text, out currentValue);

      if (AvailablePoints <= 0)
        return;
      if (currentValue >= AttributePoint.MaximumValue)
        return;

      currentValue++;
      textBox.Text = currentValue.ToString();
      AvailablePoints = AvailablePoints - 1;
    }

    private void DecrementAttribute(TextBox textBox)
    {
      int currentValue = 0;
      int.TryParse(textBox.Text, out currentValue);

      if (currentValue > 3)
      {
        currentValue--;
        textBox.Text = currentValue.ToString();
        AvailablePoints = AvailablePoints + 1;
      }
    }

    private void strengthPlusButton_Click(object sender, EventArgs e)
    {
      IncrementAttribute(strengthTextBox);
    }

    private void strengthMinusButton_Click(object sender, EventArgs e)
    {
      DecrementAttribute(strengthTextBox);
    }

    private void dexterityPlusButton_Click(object sender, EventArgs e)
    {
      IncrementAttribute(dexterityTextBox);
    }

    private void dexterityMinusButton_Click(object sender, EventArgs e)
    {
      DecrementAttribute(dexterityTextBox);
    }

    private void intelligencePlusButton_Click(object sender, EventArgs e)
    {
      IncrementAttribute(intelligenceTextBox);
    }

    private void intelligenceMinusButton_Click(object sender, EventArgs e)
    {
      DecrementAttribute(intelligenceTextBox);
    }

    private void wisdomPlusButton_Click(object sender, EventArgs e)
    {
      IncrementAttribute(wisdomTextBox);
    }

    private void wisdomMinusButton_Click(object sender, EventArgs e)
    {
      DecrementAttribute(wisdomTextBox);
    }

    private void charismaPlusButton_Click(object sender, EventArgs e)
    {
      IncrementAttribute(charismaTextBox);
    }

    private void charismaMinusButton_Click(object sender, EventArgs e)
    {
      DecrementAttribute(charismaTextBox);
    }

    private void constitutionPlusButton_Click(object sender, EventArgs e)
    {
      IncrementAttribute(constitutionTextBox);
    }

    private void constitutionMinusButton_Click(object sender, EventArgs e)
    {
      DecrementAttribute(constitutionTextBox);
    }

    private void nameTextBox_Validating(object sender, CancelEventArgs e)
    {
      string name = nameTextBox.Text;
      if (Globals.DbStorage.PlayerCharacterRepo.PlayerCharacterByNameExists(name))
      {
        e.Cancel = true;
        nameTextBox.Focus();
        theErrorProvider.SetError(nameTextBox, "Player name already exists.");
      }
      else if (0 == string.Compare(name, UnnamedName, StringComparison.InvariantCultureIgnoreCase))
      {
        e.Cancel = true;
        nameTextBox.Focus();
        theErrorProvider.SetError(nameTextBox, "Enter a player name.");
      }
      else
      {
        e.Cancel = false;
        theErrorProvider.SetError(nameTextBox, string.Empty);
      }
    }

    private void availablePointsTextBox_Validating(object sender, CancelEventArgs e)
    {
      if (AvailablePoints > 0)
      {
        e.Cancel = true;
        strengthTextBox.Focus();
        theErrorProvider.SetError(availablePointsTextBox, "Unallocated Ability Points.");
      }
    }
  }
}
