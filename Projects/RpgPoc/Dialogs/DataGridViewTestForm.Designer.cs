﻿namespace RpgPoc.Dialogs
{
  partial class DataGridViewTestForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.theObjectListView = new BrightIdeasSoftware.ObjectListView();
      ((System.ComponentModel.ISupportInitialize)(this.theObjectListView)).BeginInit();
      this.SuspendLayout();
      // 
      // theObjectListView
      // 
      this.theObjectListView.CellEditUseWholeCell = false;
      this.theObjectListView.HideSelection = false;
      this.theObjectListView.Location = new System.Drawing.Point(12, 12);
      this.theObjectListView.Name = "theObjectListView";
      this.theObjectListView.Size = new System.Drawing.Size(776, 346);
      this.theObjectListView.TabIndex = 0;
      this.theObjectListView.UseCompatibleStateImageBehavior = false;
      this.theObjectListView.View = System.Windows.Forms.View.Details;
      // 
      // DataGridViewTestForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(800, 450);
      this.Controls.Add(this.theObjectListView);
      this.Name = "DataGridViewTestForm";
      this.Text = "DataGridViewTestForm";
      ((System.ComponentModel.ISupportInitialize)(this.theObjectListView)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private BrightIdeasSoftware.ObjectListView theObjectListView;
  }
}