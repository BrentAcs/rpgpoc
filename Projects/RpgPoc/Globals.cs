﻿using Microsoft.Extensions.Configuration;
using RpgPoc.Core;
using RpgPoc.Core.Storage;

namespace RpgPoc
{
  //Singleton?

  static class Globals
  {
    private static IConfiguration _configuration;
    private static IDbStorage _dbStorage;

    public static IConfiguration Configuration
    {
      get
      {
        if (null == _configuration)
        {
          _configuration = new ConfigurationBuilder()
            .AddJsonFile(@"appsettings.json", optional: true)
            .Build();
        }

        return _configuration;
      }
    }

    public static IDbStorage DbStorage
    {
      get
      {
        if (null == _dbStorage)
        {
          _dbStorage = new DbStorage( Configuration[ConfigKeys.MongoDbConnectionString], Configuration[ConfigKeys.MongoDbDatabaseName]);
        }

        return _dbStorage;
      }
    }
  }
}