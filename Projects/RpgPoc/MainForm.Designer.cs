﻿namespace RpgPoc
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
      this.mainToolStrip = new System.Windows.Forms.ToolStrip();
      this.createSampleCharactersToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.mainStatusStrip = new System.Windows.Forms.StatusStrip();
      this.mainVerticalSplitContainer = new System.Windows.Forms.SplitContainer();
      this.topHorizontalSplitContainer = new System.Windows.Forms.SplitContainer();
      this.thePartyListView = new RpgPoc.Controls.PartyListView();
      this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
      this.mainToolStrip.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.mainVerticalSplitContainer)).BeginInit();
      this.mainVerticalSplitContainer.Panel1.SuspendLayout();
      this.mainVerticalSplitContainer.Panel2.SuspendLayout();
      this.mainVerticalSplitContainer.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.topHorizontalSplitContainer)).BeginInit();
      this.topHorizontalSplitContainer.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainMenuStrip
      // 
      this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
      this.mainMenuStrip.Name = "mainMenuStrip";
      this.mainMenuStrip.Size = new System.Drawing.Size(873, 24);
      this.mainMenuStrip.TabIndex = 0;
      // 
      // mainToolStrip
      // 
      this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createSampleCharactersToolStripButton,
            this.toolStripButton1});
      this.mainToolStrip.Location = new System.Drawing.Point(0, 24);
      this.mainToolStrip.Name = "mainToolStrip";
      this.mainToolStrip.Size = new System.Drawing.Size(873, 25);
      this.mainToolStrip.TabIndex = 1;
      // 
      // createSampleCharactersToolStripButton
      // 
      this.createSampleCharactersToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.createSampleCharactersToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("createSampleCharactersToolStripButton.Image")));
      this.createSampleCharactersToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.createSampleCharactersToolStripButton.Name = "createSampleCharactersToolStripButton";
      this.createSampleCharactersToolStripButton.Size = new System.Drawing.Size(23, 22);
      this.createSampleCharactersToolStripButton.Text = "Create Chars";
      this.createSampleCharactersToolStripButton.Click += new System.EventHandler(this.createSampleCharactersToolStripButton_Click);
      // 
      // mainStatusStrip
      // 
      this.mainStatusStrip.Location = new System.Drawing.Point(0, 531);
      this.mainStatusStrip.Name = "mainStatusStrip";
      this.mainStatusStrip.Size = new System.Drawing.Size(873, 22);
      this.mainStatusStrip.TabIndex = 2;
      // 
      // mainVerticalSplitContainer
      // 
      this.mainVerticalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainVerticalSplitContainer.Location = new System.Drawing.Point(0, 49);
      this.mainVerticalSplitContainer.Name = "mainVerticalSplitContainer";
      this.mainVerticalSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // mainVerticalSplitContainer.Panel1
      // 
      this.mainVerticalSplitContainer.Panel1.Controls.Add(this.topHorizontalSplitContainer);
      // 
      // mainVerticalSplitContainer.Panel2
      // 
      this.mainVerticalSplitContainer.Panel2.Controls.Add(this.thePartyListView);
      this.mainVerticalSplitContainer.Size = new System.Drawing.Size(873, 482);
      this.mainVerticalSplitContainer.SplitterDistance = 265;
      this.mainVerticalSplitContainer.TabIndex = 3;
      // 
      // topHorizontalSplitContainer
      // 
      this.topHorizontalSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
      this.topHorizontalSplitContainer.Location = new System.Drawing.Point(0, 0);
      this.topHorizontalSplitContainer.Name = "topHorizontalSplitContainer";
      this.topHorizontalSplitContainer.Size = new System.Drawing.Size(873, 265);
      this.topHorizontalSplitContainer.SplitterDistance = 335;
      this.topHorizontalSplitContainer.TabIndex = 0;
      // 
      // thePartyListView
      // 
      this.thePartyListView.Dock = System.Windows.Forms.DockStyle.Fill;
      this.thePartyListView.Location = new System.Drawing.Point(0, 0);
      this.thePartyListView.Name = "thePartyListView";
      this.thePartyListView.Size = new System.Drawing.Size(873, 213);
      this.thePartyListView.TabIndex = 0;
      // 
      // toolStripButton1
      // 
      this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
      this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.toolStripButton1.Name = "toolStripButton1";
      this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
      this.toolStripButton1.Text = "toolStripButton1";
      this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(873, 553);
      this.Controls.Add(this.mainVerticalSplitContainer);
      this.Controls.Add(this.mainStatusStrip);
      this.Controls.Add(this.mainToolStrip);
      this.Controls.Add(this.mainMenuStrip);
      this.Name = "MainForm";
      this.Text = "Rpg Poc - Gui";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.mainToolStrip.ResumeLayout(false);
      this.mainToolStrip.PerformLayout();
      this.mainVerticalSplitContainer.Panel1.ResumeLayout(false);
      this.mainVerticalSplitContainer.Panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.mainVerticalSplitContainer)).EndInit();
      this.mainVerticalSplitContainer.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.topHorizontalSplitContainer)).EndInit();
      this.topHorizontalSplitContainer.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStrip mainToolStrip;
        private System.Windows.Forms.StatusStrip mainStatusStrip;
        private System.Windows.Forms.SplitContainer mainVerticalSplitContainer;
        private System.Windows.Forms.SplitContainer topHorizontalSplitContainer;
    private System.Windows.Forms.ToolStripButton createSampleCharactersToolStripButton;
    private Controls.PartyListView thePartyListView;
    private System.Windows.Forms.ToolStripButton toolStripButton1;
  }
}

