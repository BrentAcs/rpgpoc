﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgPoc
{
  public partial class MainForm
  {
    private void LoadSettings()
    {
      Location = Properties.Settings.Default.MainFormLocation;
      Size = Properties.Settings.Default.MainFormSize;

      mainVerticalSplitContainer.SplitterDistance = Properties.Settings.Default.MainVerticalSplitContainer_SplitterDistance;
      topHorizontalSplitContainer.SplitterDistance = Properties.Settings.Default.TopHorizontalSplitContainer_SplitterDistance;

      thePartyListView.LoadSettings();

      //thePartyListView.Columns[0].Width = Properties.Settings.Default.PartyListView_NameColumnHeader_Width;
      //thePartyListView.Columns[1].Width = Properties.Settings.Default.PartyListView_LevelColumnHeader_Width;
      //thePartyListView.Columns[2].Width = Properties.Settings.Default.PartyListView_CurrentHitPointsColumnHeader_Width;
      //thePartyListView.Columns[3].Width = Properties.Settings.Default.PartyListView_MaximumHitPointsColumnHeader_Width;
      //thePartyListView.Columns[4].Width = Properties.Settings.Default.PartyListView_ArmorClassColumnHeader_Width;
      //thePartyListView.Columns[5].Width = Properties.Settings.Default.PartyListView_ClassColumnHeader_Width;
    }

    private void SaveSettings()
    {
      Properties.Settings.Default.MainFormLocation = Location;
      Properties.Settings.Default.MainFormSize = Size;

      Properties.Settings.Default.MainVerticalSplitContainer_SplitterDistance = mainVerticalSplitContainer.SplitterDistance;
      Properties.Settings.Default.TopHorizontalSplitContainer_SplitterDistance = topHorizontalSplitContainer.SplitterDistance;

      thePartyListView.SaveSettings();

      //Properties.Settings.Default.PartyListView_NameColumnHeader_Width = thePartyListView.Columns[0].Width;
      //Properties.Settings.Default.PartyListView_LevelColumnHeader_Width = thePartyListView.Columns[1].Width;
      //Properties.Settings.Default.PartyListView_CurrentHitPointsColumnHeader_Width = thePartyListView.Columns[2].Width;
      //Properties.Settings.Default.PartyListView_MaximumHitPointsColumnHeader_Width = thePartyListView.Columns[3].Width;
      //Properties.Settings.Default.PartyListView_ArmorClassColumnHeader_Width = thePartyListView.Columns[4].Width;
      //Properties.Settings.Default.PartyListView_ClassColumnHeader_Width = thePartyListView.Columns[5].Width;

      Properties.Settings.Default.Save();
    }
  }
}
