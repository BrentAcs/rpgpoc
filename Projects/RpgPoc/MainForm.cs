﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Configuration;
using RpgPoc.Core;
using RpgPoc.Core.Characters;
using RpgPoc.Core.Extensions;
using RpgPoc.Core.Storage;
using RpgPoc.Dialogs;

namespace RpgPoc
{
  public partial class MainForm : Form
  {
    public MainForm()
    {
      InitializeComponent();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      // Nuke for testing each run.
      Globals.DbStorage.DropAllCollections();

      LoadSettings();

      // for now, hide main menu strip, not sure if I want it, but don't want to muck w/ UI later if I do.
    }

    private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      SaveSettings();
    }

    private void createSampleCharactersToolStripButton_Click(object sender, EventArgs e)
    {
      CreateCharacterForm createCharacterForm = new CreateCharacterForm();
      if (DialogResult.OK != createCharacterForm.ShowDialog(this))
        return;

      thePartyListView.AddPlayerCharacter(createCharacterForm.PlayerCharacter);

      Globals.DbStorage.PlayerCharacterRepo.InsertOne(createCharacterForm.PlayerCharacter);
    }

    private void toolStripButton1_Click(object sender, EventArgs e)
    {
      var dialog = new DataGridViewTestForm();
      dialog.ShowDialog(this);
    }

    public void CreateStockPlayerCharactersForTesting()
    {
      List<PlayerCharacter> pcs = new List<PlayerCharacter>
      {
        new PlayerCharacter()
        {
          Name = "Gotch Urarse",
          Class = CharacterClass.Fighter,
        },
        new PlayerCharacter()
        {
          Name = "Empathy Rests",
          Class = CharacterClass.Wizard,
        }
      };

      pcs[0].ApplyXp(5000);
      pcs[1].ApplyXp(20000);

    //  thePartyListView.Items.Clear();
    //  foreach (var playerCharacter in pcs)
    //  {
    //    thePartyListView.AddPlayerCharacter(playerCharacter);
    //  }
    }
  }
}
